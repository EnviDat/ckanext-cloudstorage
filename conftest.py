# -*- coding: utf-8 -*-

pytest_plugins = [
    u'ckanext.cloudstorage.tests.ckan_setup',
    u'ckanext.cloudstorage.tests.fixtures',
]
